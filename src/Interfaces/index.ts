export interface IApiResponse {
     airdate: string;
     answer: string;
     category: IQuestionCategory;
     category_id: number;
     created_at: string;
     game_id: number;
     id: number;
     invalid_count: number | null;
     question: string;
     updated_at: string;
     value: number;
}
export interface IQuestionCategory {
     clues_count: number;
     created_at: string;
     id: number;
     title: string;
     updated_at: string;
}
export interface IQuestionAnswer {
     answer: string;
     question: string;
}

export enum AnswerState {
     CORRECT = 'CORRECT',
     WRONG = 'WRONG',
}

export interface ResultProps {
     answerState: AnswerState | null;
     getNextQuestion: () => void
     question: IQuestionAnswer | null;
}
export interface QuestionAnswerProps {
     answerState: AnswerState | null;
     inputHandler: (e: any) => void;
     question: IQuestionAnswer | null;
     submitAnswer: () => void;
     userAnswer: string | null;
}