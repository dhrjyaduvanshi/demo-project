import React from 'react';
import './App.css';
import { QuestionAnswerProps } from '../Interfaces';

function QuestionAnswer(props: QuestionAnswerProps) {
     return (
          <div className="page-content">
          <div className="question-container padding-16">
            <b>Question:</b> {props.question?.question} ?
          </div>
          <div className="answer-container padding-16">
            <input disabled={!!props.answerState} type="text" onKeyUp={props.inputHandler} className="answer-input" placeholder="answer..."/>
            <button disabled={!props.userAnswer} className="submit-button" onClick={props.submitAnswer}>Submit</button>
          </div>
        </div>
     );
   }
export default QuestionAnswer;