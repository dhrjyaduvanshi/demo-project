import React from 'react';
import './App.css';
import { ResultProps, AnswerState } from '../Interfaces';

function Result(props: ResultProps) {
     return (
       <div className="page-content">
             <div className="padding-16">
             <p>Your answer is <b>{props.answerState?.toLowerCase()}</b></p>
             {props.answerState === AnswerState.WRONG ? <p>Correct answer is: "<b>{ props.question?.answer }</b>"</p> : null}
               <button className="next-button" onClick={props.getNextQuestion}>Next Question</button>
             </div>
           </div>
     );
   }
export default Result;