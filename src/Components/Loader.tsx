import React from 'react';
import loaderImage from '../Assets/Images/loader.gif';
import  './Loader.css';
export function Loader() {
     return (
          <div className="loader-container">
               <img src={loaderImage}  className="loader-image"/>
          </div>
     )
}