import React, { useEffect, useState } from 'react';
import './Css/App.css';
import axios, { AxiosResponse } from "axios";
import { Loader } from './Components/Loader';
import { IApiResponse, IQuestionAnswer, AnswerState } from './Interfaces';
import Result from "./Components/Result";
import QuestionAnswer from './Components/QuestionAnswer';

function App() {
  const [question, setQuestion] = useState<IQuestionAnswer | null>(null);
  const [isLoading, setLoading] = useState<boolean>(false);
  const [userAnswer, setUserAnswer] = useState<string | null>(null);
  const [answerState, setAnswerState] = useState<AnswerState | null>(null);
  
  useEffect(() => {
    getQuestions();
  }, []);

  const getQuestions = (): void => {
    setLoading(true);
    axios.get(`https://jservice.io/api/random`).then( (response:AxiosResponse<IApiResponse[]>) => {
      const { question, answer } = response.data[0];
      setQuestion({ question, answer });
      setLoading(false);
    }).catch(error => {
      setLoading(false);
    })
  }
  const inputHandler =(e: any): void => {
    setUserAnswer(e.target?.value || null);
  }
  const submitAnswer = (): void => {
    if (userAnswer?.toLowerCase() === question?.answer?.toLowerCase()) {
      setAnswerState(AnswerState.CORRECT);
    } else {
      setAnswerState(AnswerState.WRONG);
    }
  }
  const getNextQuestion = (): void => {
    setQuestion(null);
    setUserAnswer(null);
    setAnswerState(null);
    getQuestions();

  }
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Quiz
        </p>
      </header>
      {isLoading ? <Loader/>: null}
      {question ? <QuestionAnswer
        answerState={answerState}
        question={question}
        userAnswer={userAnswer}
        inputHandler={inputHandler}
        submitAnswer={submitAnswer}
      /> : null}
      {answerState ? <Result question={question} answerState={answerState} getNextQuestion={getNextQuestion}/>: null}
    </div>
  );
}

export default App;
